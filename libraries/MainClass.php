<?php
/**
 * MainClass
 */
class MainClass 
{
	
	var $servername = "localhost";
	var $username = "root";
	var $password = "";
	var $database = "camara";


	function __construct()
	{

		$servername = $this->servername ;
		$username   = $this->username   ;
		$password   = $this->password   ;
		$database   = $this->database   ;

		// Create connection
		$conn = mysqli_connect($servername, $username, $password);
		// Check connection
		if (!$conn) {
		    die("Connection failed: " . mysqli_connect_error());
		}

		// Create database
		if (!mysqli_select_db($conn,$database)) {
			$sql = "CREATE DATABASE IF NOT EXISTS $database";
			if (mysqli_query($conn, $sql)) {
			    echo "Database created successfully <br/>";
			} else {
			    echo "Could not create database: " . mysqli_error($conn);
			}
		} else {
		    // echo "Database already exists: " . mysqli_error($conn);
		}

		// make $database the current db
		$db_selected = mysqli_select_db($conn, $database);
		if (!$db_selected) {
		    die ('Can\'t use database : ' . mysqli_error($conn));
		} else {
		    // echo "Database Selected <br/>";
		}


		$select_table = "SELECT id FROM user";
		$result = mysqli_query($conn, $select_table);

		// sql to create table if not exist
		if(empty($result)) {
			$sql = "CREATE TABLE user( 
			    id INT NOT NULL AUTO_INCREMENT, 
			    username VARCHAR(40) NOT NULL, 
			    password VARCHAR(50) NOT NULL, 
			    PRIMARY KEY (`id`)
			)";


			$retval = mysqli_query($conn, $sql );
			if(! $retval ) {
			    die('Could not create table: ' . $conn->error);
			}

			echo "Table created successfully<br/>";

			$sql = "INSERT INTO user (username, password) VALUES ('user', md5('user'));";
			$sql .= "INSERT INTO user (username, password) VALUES ('user1', md5('user1'));";
			$sql .= "INSERT INTO user (username, password) VALUES ('user2', md5('user2'));";
			$sql .= "INSERT INTO user (username, password) VALUES ('user3', md5('user3'));";

			if (mysqli_multi_query($conn,$sql))
			{
			    echo "New record created successfully <br/>";
			} else {
			    echo "Error: " . $sql . "<br>" . $conn->error;
			}
		}		


		$select_table = "SELECT id FROM camara";
		$result = mysqli_query($conn, $select_table);

		// sql to create camara table if not exist
		if(empty($result)) {
			$camara_sql = "CREATE TABLE camara( 
			    id INT NOT NULL AUTO_INCREMENT, 
			    name VARCHAR(40) NOT NULL, 
			    image VARCHAR(50) NOT NULL, 
			    PRIMARY KEY (`id`)
			)";

			$retval = mysqli_query($conn, $camara_sql );
			if(! $retval ) {
			    die('Could not create table: ' . $conn->error);
			}

			//echo "Table created successfully<br/>";
		}		



		$review_table = "SELECT id FROM review";
		$result_review = mysqli_query($conn, $review_table);

		// sql to create review table if not exist
		if(empty($result_review)) {
			$review_sql = "CREATE TABLE review( 
			    id INT NOT NULL AUTO_INCREMENT, 
			    camara_id INT NOT NULL , 
			    review VARCHAR(255) NOT NULL, 
			    reviewer_name VARCHAR(255) NOT NULL, 
			    rating INT NOT NULL, 
			    PRIMARY KEY (`id`)
			)";


			$review = mysqli_query($conn, $review_sql );
			if(! $review ) {
			    die('Could not create table: ' . $conn->error);
			}

			// echo "Table created successfully<br/>";
		}

		$this->CONN = $conn;
		
	} //end of construct


	/**
	 * the select function is used to get data 
	 * from table 
	 */
	function select ($sql="") 
    {
		
        if(empty($sql)) { return false; }

        if(empty($this->CONN)) { return false; }
        $conn = $this->CONN;
        $results = mysqli_query($conn, $sql);
        if( (!$results) or (empty($results)) ) { 
            return false;
        }
        $count = 0;
        $data = array();

        while ( $row = mysqli_fetch_array($results,MYSQLI_ASSOC))
        {
            $data[$count] = $row;
            $count++; 
        }
        return $data;
    }

	/**
	 * the insert function is used to insert data 
	 * into the table 
	 */
    function insert ($sql="")
    {
        if(empty($sql)) { return false; }
        if(empty($this->CONN))
        {
            return false;
        }
        $conn = $this->CONN;

		if ($conn->query($sql) === TRUE) {
			$last_id = $conn->insert_id;
        	return $last_id;
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
			return false;
		}
    }

	/**
	 * the delete function is used to delete row 
	 * from table 
	 */

    function delete($sql)
    {

        if(empty($sql)) { return false; }
        if(empty($this->CONN))
        {
            return false;
        }
        $conn = $this->CONN;

		if ($conn->query($sql) === TRUE) {
			echo "Record deleted successfully";
		}  else {
			echo "Error deleting record: " . mysqli_error($conn);
		}

    }

    /**
	 * the drop function is used to drop 
	 * specific table from database
	 */

    function drop_table($table){
    	$sql = "DROP TABLE ".$table;

    	if(empty($sql)) { return false; }
        if(empty($this->CONN))
        {
            return false;
        }
        $conn = $this->CONN;

		if ($conn->query($sql) === TRUE) {
			echo "Table deleted successfully";
		}  else {
			echo "Error deleting Table: " . mysqli_error($conn);
		}
    }

}

/**
 * Create object of MainClass
 */

if(!isset($obj)){
    $obj = new MainClass();
}