<?php
  session_start();
  include('../libraries/MainClass.php');

  $query = "select * from camara";
  $camaras = $obj->select($query);

  if($_SESSION['username'] == '') {
  	echo "<script> window.location.href = 'login.php' </script>";	
  }

  include('includes/header.php');
?>

<div class="camara-form">
	<div class="camara-form-heading">Provide your information</div>
	<form action="form_submit.php" method="post" enctype="multipart/form-data">
		<label for="name"><span>Name <span class="required">*</span></span><input type="text" class="input-field" name="name" /></label>

		<label for="image"><span>Image <span class="required">*</span></span><input type="file" class="input-field" name="image" /></label>
		<label><span>&nbsp;</span><input type="submit"  name="camara-form-submit" value="Submit" /></label>
	</form>
</div>

<div class="camara-listing">
  <div class="camara-listing-heading">Camara Listing</div>
  <ul>
  <?php 
  		foreach ($camaras as $key => $value) {
  ?>
    <li>
      <img src="images/camara/<?php echo $value['image']?>" width="100px;"/>
      <h3><a href="camara-details.php/<?php echo $value['id']?>"><?php echo $value['name']?></a></h3>
    </li>
  <?php } ?>
  </ul>
</div>

<?php
include('includes/footer.php');