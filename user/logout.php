<?php 
	session_start();
	include('../libraries/MainClass.php');

	unset($_SESSION['username']);
	unset($_SESSION['userid']);
	header("Location: login.php");

?>