<?php
  session_start();
  include('../libraries/MainClass.php');

  $_SERVER['REQUEST_URI_PATH'] = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
  $segments = explode('/', $_SERVER['REQUEST_URI_PATH']);

  $query = "select * from camara WHERE id ='".$segments[4]."'";
  $camara = $obj->select($query);

  $review_query = "select * from review WHERE camara_id ='".$segments[4]."'";
  $reviews = $obj->select($review_query);

  if($segments[4] == '' || empty($camara)) {
  	echo "Sorry, incorrect link.";
  	die();
  }

  include('includes/header.php');

?>

<div class="header">
<h1>Camara Details</h1>
</div>

<div class="row">

<h1><?php echo $camara[0]['name']?></h1>
<img src="http://<?php echo $_SERVER['SERVER_NAME'] ?>/camara/user/images/camara/<?php echo $camara[0]['image']?>" width="100px;"/>


<div class="reviews">
  <h3>Reviews</h3>
  <?php
      $already_reviewed = '';

      if(!empty($reviews)){

      foreach ($reviews as $key => $value) {

        if($value['reviewer_name'] == $_SESSION['username']){
          $already_reviewed = '1';
        }
  ?>
  <div class="review">
    <div class="reviewer-name">Reviewer Name: <?php echo $value['reviewer_name']; ?></div>
    <div class="reviewer-review">Review: <?php echo $value['review']; ?></div>
    <div class="reviewer-rating">Rating: <?php echo $value['rating']; ?></div> 
  </div>
  <br/>

  <?php 
      }
    }else{
  ?>
    No reviews
  <?php 
    }
  ?>


</div>



<?php

if(isset($_SESSION['username'])){ 

  if($already_reviewed!=1 && $camara!= $_SESSION['userid']){

?>
<div class="review-form">
  <h2>Camara Review</h2>
  <div>Review camara with comment</div>
    
  <div>
    <label class="desc" id="title4" for="review-text">
      Comment:
    </label>
  
    <div>
      <textarea id="review-text" name="review-text" spellcheck="true" rows="10" cols="50" tabindex="4"></textarea>
    </div>
  </div>
  
  <div>
    <label class="desc">
    	Rate:
    </label>
    <div>
    <select id="review-rate" name="review-rate" class="field select medium" tabindex="11"> 
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
      <option value="6">6</option>
      <option value="7">7</option>
      <option value="8">8</option>
      <option value="9">9</option>
      <option value="10">10</option>
    </select>
    </div>
  </div>
  
  <div>
		<div>
      <input type="hidden" value="<?php echo $segments[4]; ?>" id="camara-id">
  		<input type="button" value="Submit" id="review-submit">
    </div>
	</div>
</div>
<!-- end of  review form div -->

<?php 

}}
else{

?>
Please <a href= "http://<?php echo $_SERVER['SERVER_NAME'] ?>/camara/user/login.php">Login</a> to Review
<?php } ?>

</div>

<?php
include('includes/footer.php');