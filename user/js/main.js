$(function () {
	$('#review-submit').on('click', function(){
		var rating = $('#review-rate').val();
		var review = $('#review-text').val();
		var camara_id = $('#camara-id').val();

		if(review!=''){
			$.ajax({
				type:"post",
				data: {rating:rating, review:review, camara_id:camara_id},
				url: '/camara/user/form_submit.php',    
				beforeSend: function(){

				},
				success: function(output_string){ 

					 var json = $.parseJSON(output_string);
					 var div = '<div class="review">'+
							    '<div class="reviewer-name">Reviewer Name: '+json.reviewer_name+'</div>'+
							    '<div class="reviewer-review">Review: '+json.review+'</div>'+
							    '<div class="reviewer-rating">Rating: '+json.rating+'</div>'+ 
							  '</div>'+
							  '<br/>';

					$('.reviews').append(div);
					$('.review-form').html('Thanks for Review');


				}
			}); 

		}else{
			alert('Review can not be empty');
		}



	});
});