<!DOCTYPE html>
<html>
<head>
	<title>Camara | Home</title>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<script src="../js/jquery.js"></script>
	<script src="../js/main.js"></script>
</head>
<body>
<header>

<?php if(isset($_SESSION['username'])){?>

	<div class="logged-user">Welcome <span class="user-name"><?php echo $_SESSION['username'];?></span></div>
	<div class="logout"> <a href="http://<?php echo $_SERVER['SERVER_NAME'] ?>/camara/user/logout.php">Logout</a> </div>

<?php } ?>

</header>
	<div class="main-class">