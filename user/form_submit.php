<?php
session_start();
include('../libraries/MainClass.php');

// login form submit start
if($_POST['login_submit']){
	$user = $_POST["username"];
	$password = md5($_POST["password"]);
	$query = "select * from user where username='".$user."' and password= '".$password."'";
	$data = $obj->select($query);

	if(count($data)>0){
		$_SESSION['username'] = $data[0]['username'];
		$_SESSION['userid'] = $data[0]['id'];
		header("location:index.php");
	}else{
		$_SESSION['error_message'] = 'Incorrect username or password.';
		header("location:login.php");		
	}
}
// login form submit end

// camara form submit start
if($_POST['camara-form-submit']){
	if($_FILES["image"]["name"]){
		$target_dir = "images/camara/";
		$target_file = $target_dir . basename($_FILES["image"]["name"]);
		$uploadOk = 1;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
		// Check if image file is a actual image or fake image
		if(isset($_POST["submit"])) {
		    $check = getimagesize($_FILES["image"]["tmp_name"]);
		    if($check !== false) {
		        echo "File is an image - " . $check["mime"] . ".";
		        $uploadOk = 1;
		    } else {
		        echo "File is not an image.";
		        $uploadOk = 0;
		    }
		}
		// Check if file already exists
		if (file_exists($target_file)) {
		    echo "Sorry, file already exists.";
		    $uploadOk = 0;
		}
		// Check file size
		if ($_FILES["image"]["size"] > 50000000) { //500000 = 500 kb
		    echo "Sorry, your file is too large.";
		    $uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {
		    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		    $uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    echo "Sorry, your file was not uploaded.";
		// if everything is ok, try to upload file
		} else {
		    if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
		        // echo "The file ". basename( $_FILES["image"]["name"]). " has been uploaded.";
		    } else {
		        echo "Sorry, there was an error uploading your file.";
		    }
		}
	}

	$name = $_POST['name'];
	$image = $_FILES["image"]["name"];
	$user_id = $_SESSION['userid'];

	$sql = "INSERT INTO camara (user_id, name, image)
	VALUES ('".$user_id."', '".$name."', '".$image."')";

	$result = $obj->insert($sql);

	if($result){
		header("location:index.php");
	}

}
// camara form submit end

// review submit start
if($_POST['review'] && $_POST['review']!=''){
	$camara_id = $_POST["camara_id"];
	$rating = $_POST["rating"];
	$review = $_POST["review"];
	$reviewer_name = $_SESSION['username'];
	$sql = "INSERT INTO review (camara_id, rating, review, reviewer_name)
	VALUES ('".$camara_id."', '".$rating."', '".$review."', '".$reviewer_name."')";

	$result = $obj->insert($sql);
	if($result){
		$result = array(
			'rating' => $rating, 
			'review' => $review, 
			'camara_id' => $camara_id,
			'reviewer_name' => $reviewer_name
			);

		$data = json_encode($result);
		echo $data;		
	}
}

// review submit end


